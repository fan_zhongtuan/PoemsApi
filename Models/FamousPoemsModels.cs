﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Models
{
    public class FamousPoemsModels
    {
        public FamousPoemsModels()
        {

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string vcAuthor { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string vcContent { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string vcTitle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string vcDesc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime dtAddTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime dtCreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime dtUpdateTime { get; set; }
    }
}
