﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Services;

namespace WebApi.Controllers
{

    public class PoemsController : ControllerBase
    {
        private readonly FamousPoemsService _poemsService;
        public PoemsController(FamousPoemsService poemsService)
        {
            _poemsService = poemsService;
        }
        [HttpPost("Poems/Insert")]
        public ActionResult Insert([FromBody]FamousPoemsModels poems)
        {

            poems.dtCreateTime = DateTime.Now;
            poems.dtUpdateTime = DateTime.Now;
            return Content(JsonConvert.SerializeObject(_poemsService.Insert(poems)));
        }
        [HttpGet("Poems/InsertTest")]
        public ActionResult InsertTest()
        {
            FamousPoemsModels poems = new FamousPoemsModels();
            poems.vcAuthor = "· s 恩";
            poems.vcContent = "人皆养子望聪明，我被聪明误一生。惟愿孩儿愚且鲁，无灾无难到公卿。";
            poems.vcTitle = "";
            poems.vcDesc = null;
            poems.dtCreateTime = DateTime.Now;
            poems.dtUpdateTime = DateTime.Now;
            return Content(JsonConvert.SerializeObject(_poemsService.Insert(poems)));
        }
        [HttpPost("Poems/InsertManyAsync")]
        public ActionResult InsertManyAsync([FromBody]List<FamousPoemsModels> poems)
        {
            return Content(JsonConvert.SerializeObject(_poemsService.InsertManyAsync(poems)));
        }
        [HttpPost("Poems/Update")]
        public ActionResult Update([FromBody]FamousPoemsModels poems)
        {
            _poemsService.Update(poems);
            return NoContent();
        }
        [HttpPost("Poems/Delete")]
        public ActionResult Delete([FromBody]FamousPoemsModels poems)
        {
            _poemsService.Delete(poems);
            return NoContent();
        }
        [HttpGet("Poems/Get")]
        public ActionResult Get(string id)
        {
            return Content(JsonConvert.SerializeObject(_poemsService.Get(id)));
        }
        [HttpGet("Poems/List")]
        public ActionResult GetPageList(int nPageIndex = 1, int nPageSize = 10)
        {
            return Content(JsonConvert.SerializeObject(_poemsService.GetPageList(nPageIndex, nPageSize)));
        }
    }
}