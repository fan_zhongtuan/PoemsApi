﻿using Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class FamousPoemsService
    {
        private readonly IMongoCollection<FamousPoemsModels> _mongDb;

        public FamousPoemsService(IMongdbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _mongDb = database.GetCollection<FamousPoemsModels>(settings.FamousPoemsCollectionName);
        }
        public FamousPoemsModels Insert(FamousPoemsModels model)
        {
            _mongDb.InsertOne(model);
            return model;
        }
        public int InsertManyAsync(List<FamousPoemsModels> list)
        {
            _mongDb.InsertManyAsync(list);
            return list.Count;
        }
        public void Update(FamousPoemsModels model)
        {
            _mongDb.ReplaceOne(m => m.Id == model.Id, model);
        }
        public void Delete(FamousPoemsModels model)
        {
            _mongDb.DeleteOne(m => m.Id == model.Id);
        }
        public FamousPoemsModels Get(string id)
        {
            return _mongDb.Find<FamousPoemsModels>(book => book.Id == id).FirstOrDefault();
        }
        public List<FamousPoemsModels> GetPageList(int nPageIndex = 1, int nPageSize = 10)
        {

            FilterDefinition<FamousPoemsModels> filter = Builders<FamousPoemsModels>.Filter.Eq("vcAuthor", "李白");
            var list = new List<FieldDefinition<FamousPoemsModels>>();
  
            //list.Add(Builders<FamousPoemsModels>.Filter.Where(t => t.vcAuthor == "李白"));
            //var filter = Builders<FamousPoemsModels>.Filter.And(list);
            long nCount = _mongDb.Find<FamousPoemsModels>(b => true).CountDocuments();
            //设置排序字段
            var sort = Builders<FamousPoemsModels>.Sort.Descending(it => it.dtCreateTime);
            return _mongDb.Find<FamousPoemsModels>(b =>string.IsNullOrEmpty(b.vcDesc)).Sort(sort).Skip((nPageIndex - 1) * nPageSize).Limit(nPageSize).ToList();
        }


    }
}
