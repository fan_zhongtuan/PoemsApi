﻿using System;

namespace Services
{
    public class MongdbSettings : IMongdbSettings
    {
        public string FamousPoemsCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IMongdbSettings
    {
        public string FamousPoemsCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
